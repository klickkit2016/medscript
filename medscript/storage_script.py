import json
import marisa_trie
import sys
import os
import django
import re

from django.db import IntegrityError

sys.path.append('/Users/sriablaze/Desktop/Projects/MS/MS')
os.environ['DJANGO_SETTINGS_MODULE'] = 'MS.settings'
django.setup()

from medscript.models import Drug, DrugQuantity

FINAL_PATTERN_STRING = '(?<=[/+\(])\s*\d+(?:\.\d+)?\s*\w*\s*(?=[\)+/])'
UNIT_CHECK_STRING = re.compile('[A-Za-z]+')


def byteify(input):
    if isinstance(input, dict):
        return {byteify(key): byteify(value)
                for key, value in input.iteritems()}
    elif isinstance(input, list):
        return [byteify(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input

MG_KEY = "mg"

def store_data_in_model(medicine_list):
    Drug.objects.all().delete()
    medicine_dict
    trigger_drug_dict = {}
    for medicine_dict in medicine_list:
        for sno, medicine_name in medicine_dict.items():
            actual_medicine_name = medicine_name
            medicine_name = medicine_name.replace(" ", "")
            if medicine_name not in trigger_drug_dict.keys():
                trigger_drug_dict[medicine_name] = None
                Drug.objects.create(medicine_name=actual_medicine_name)
            else:
                with open("repetitions.txt", "a") as myfile:
                    myfile.write(medicine_name)
                continue

def parse_json():
    Drug.objects.all().delete()
    DrugQuantity.objects.all().delete()
    with open('medicines_with_dosage.json') as json_data:
        medicine_json = json.load(json_data)
        medicine_dict = byteify(medicine_json)
        for medicine, quantities in medicine_dict.items():
            drug_quantities_list = []
            try:
                re.sub("\s\s+", " ", medicine)
                drug = Drug.objects.create(name=medicine)
                drug.save()
            except IntegrityError as e:
                print medicine
                return
            for quantity in quantities:
                drug_quantity_list = []
                drug_quantity, created = DrugQuantity.objects.get_or_create(quantifier=quantity, defaults={'quantifier': quantity})
                drug_quantity_list.append(drug_quantity)
            drug.allowed_quantities.add(*drug_quantity_list)
           


def make_json_for_local_access():
    medicines = Drug.objects.all().values_list('id', 'medicine_name')
    medicine_dict = {}
    for medicine in medicines:
        medicine_dict[medicine[0]] = medicine[1]
    with open('drug_data_for_local_access.json', 'w') as outfile:
            json.dump(medicine_dict, outfile)


def pattern_match():
    medicines_with_dosage_units_dict = {}
    spaces_removed_medicine_name = ''
    space_removed_and_stripped_name_dict = {}
    stripped_medicine_name = ''
    search_index = 0
    medicine_matches_dict = {}
    medicines = Drug.objects.all().values_list('medicine_name')
    medicines = list(medicines)
    pattern = re.compile(FINAL_PATTERN_STRING)
    unit_check_pattern = re.compile(UNIT_CHECK_STRING)
    for medicine in medicines:
        medicine_name = medicine[0]
        matches = re.findall(pattern, medicine_name, flags=0)
        pattern_match = re.search(pattern, medicine_name)
        if pattern_match is not None:
            search_index = pattern_match.start() - 1
            stripped_medicine_name = medicine_name[:search_index].strip()
        else:
            stripped_medicine_name = medicine_name.strip()
        spaces_removed_medicine_name = stripped_medicine_name.replace(" ", "")
        if spaces_removed_medicine_name not in medicine_matches_dict.keys():
            medicine_matches_dict[spaces_removed_medicine_name] = matches
        else:
            for match in matches:
                medicine_matches_dict[spaces_removed_medicine_name].append(match)
        space_removed_and_stripped_name_dict[spaces_removed_medicine_name] = stripped_medicine_name
    print medicine_matches_dict
    for space_removed_name, stripped_name in space_removed_and_stripped_name_dict.items():
        matches = medicine_matches_dict[space_removed_name]    
        for index, match in enumerate(matches):
            space_removed_unit = match.replace(" ", "")
            pattern_match = re.search(unit_check_pattern, space_removed_unit)
            if pattern_match is None:
                matches[index] = space_removed_unit + MG_KEY
        medicines_with_dosage_units_dict[stripped_name] = matches
    with open('medicines_with_dosage.json', 'w') as outfile:
        json.dump(medicines_with_dosage_units_dict, outfile)

def autocomplete_util():
    drugs = Drug.objects.all().values_list('name')
    space_removed_drugs = [drug[0].replace(" ", "") for drug in drugs]
    trie = marisa_trie.Trie(space_removed_drugs)
    trie.save('my_trie')


def store_mapping():
    drugs = Drug.objects.all()
    result_dict = {}
    for drug in drugs:
        result_dict[drug.name.replace(" ", "")] = (drug.id, drug.name)
    with open('name_mapping.json', 'w') as outfile:
        json.dump(result_dict, outfile)


store_mapping()
'''
parse_json()
make_json_for_local_access()
'''
