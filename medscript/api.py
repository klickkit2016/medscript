# myapp/api.py
from django.conf.urls import url

from tastypie.http import HttpBadRequest
from tastypie.http import HttpUnauthorized
from tastypie.resources import ModelResource
from tastypie.utils import trailing_slash
from medscript.models import Drug

import constants
import json 
import marisa_trie
import os
import utils


class DrugResource(ModelResource):
    class Meta:
        queryset = Drug.objects.all()
        resource_name = 'drug'

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/autocomplete%s$" % 
            (self._meta.resource_name, trailing_slash()), self.wrap_view('autocomplete'), name="api_autocomplete"),
        ]

    def autocomplete(self, request, **kwargs):
        is_user_authentic, user = utils.is_user_authentic(request)
        if not is_user_authentic:
            return self.error_response(request, 
                utils.get_error_response(
                'UnAuthorized', 
                'User not allowed to access the resource'), 
                HttpUnauthorized)
        prefix_text = request.GET.get('text', None)
        if prefix_text is None:
            return self.error_response(request, 
                utils.get_error_response(
                'BadRequest', 
                'Search text not provided as parameter'), 
                HttpBadRequest)
        mapping_file_path = utils.get_file_path('name_mapping.json')
        removed_space_name_and_original_name_dict = utils.get_json_from_file(mapping_file_path)
        trie_file_path = utils.get_file_path('my_trie')
        trie = marisa_trie.Trie()
        trie.load(trie_file_path)
        response = []
        for medicine_name in trie.keys(prefix_text):
            medicine_detail_tuple = tuple(removed_space_name_and_original_name_dict[medicine_name])
            response.append({'medicine_name': medicine_detail_tuple[1], 'id': medicine_detail_tuple[0]})
        return self.create_response(request, response)