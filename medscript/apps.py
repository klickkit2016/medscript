from django.apps import AppConfig


class MedscriptConfig(AppConfig):
    name = 'medscript'

    def ready(self):
        from medscript import signals
