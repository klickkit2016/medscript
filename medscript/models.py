import json

from django.contrib.auth.models import User
from django.db import models


class Profile(models.Model):
    
    MALE_ACRONYM = 'M'
    FEMALE_ACRONYM = 'F'

    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    birth_year = models.DateField(default=None)
    latitude = models.DecimalField(max_digits=9, decimal_places=6, default=None)
    longitude = models.DecimalField(max_digits=9, decimal_places=6, default=None)


class DrugQuantity(models.Model):
    quantifier = models.CharField(max_length=5, unique=True)
    

class Drug(models.Model):
    name = models.CharField(max_length=50)
    allowed_quantities = models.ManyToManyField(DrugQuantity)
    
    def __str__(self):
        return self.name




