# myapp/api.py
from django.contrib.auth import authenticate 
from django.contrib.auth import login
from django.contrib.auth import logout
from django.contrib.auth.models import User

from django.conf.urls import url

from tastypie.authentication import ApiKeyAuthentication
from tastypie.http import HttpBadRequest
from tastypie.http import HttpNotFound
from tastypie.http import HttpUnauthorized
from tastypie.resources import ModelResource
from tastypie.utils import trailing_slash

from medscript.models import Profile
from medscript.utils import decode_json
from medscript.utils import generate_token_for_user
from medscript.utils import is_user_authentic

import constants
import utils

class AccountResource(ModelResource):
    class Meta:
        queryset = Profile.objects.all()
        resource_name = 'account'
        authentication = ApiKeyAuthentication()

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/signup%s$" % 
            (self._meta.resource_name, trailing_slash()), self.wrap_view('sign_up'), name="api_sign_up"),
            url(r"^(?P<resource_name>%s)/login%s$" % 
            (self._meta.resource_name, trailing_slash()), self.wrap_view('login'), name="api_login"),
            url(r"^(?P<resource_name>%s)/logout%s$" % 
            (self._meta.resource_name, trailing_slash()), self.wrap_view('log_user_out'), name="api_logout"),
        ]


    def login(self, request, **kwargs):
        login_data = decode_json(request.body)
        username = login_data[constants.USERNAME_KEY]
        password = login_data[constants.PASSWORD_KEY]
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            api_token = generate_token_for_user(user)
            return self.create_response(request, {'api_key': api_token})
        else:
            return self.error_response(request, 
                utils.get_error_response(constants.DOES_NOT_EXIST, 'UserDoesNotExist'),
                HttpBadRequest)
        

    def sign_up(self, request, **kwargs):
        signup_data = decode_json(request.body)
        username = signup_data[constants.EMAIL_KEY]
        name = signup_data[constants.NAME_KEY]
        password = signup_data[constants.PASSWORD_KEY]
        gender = signup_data['gender']
        birth_year = signup_data.get('birth_year', None)
        latitude = signup_data.get('latitude', None)
        longitude = signup_data.get('longitude', None)
        user = User.objects.create_user(username=username, email=username, password=password)
        api_token = user.api_key.key
        response_data = {
            'api_key': api_token
        }
        return self.create_response(request, response_data)


    def log_user_out(self, request, **kwargs):
        is_user_authentic, user = utils.is_user_authentic(request)
        if is_user_authentic:
            logout(request)

