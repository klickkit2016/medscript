# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-12-03 07:15
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medscript', '0003_auto_20171203_0628'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='drugquantity',
            unique_together=set([('quantifier', 'unit')]),
        ),
    ]
