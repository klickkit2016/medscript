# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-12-03 06:28
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('medscript', '0002_drug_allowed_quantities'),
    ]

    operations = [
        migrations.CreateModel(
            name='DrugQuantity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantifier', models.IntegerField()),
                ('unit', models.CharField(max_length=4)),
            ],
        ),
        migrations.RemoveField(
            model_name='drug',
            name='allowed_quantities',
        ),
        migrations.AddField(
            model_name='drug',
            name='allowed_quantities',
            field=models.ManyToManyField(to='medscript.DrugQuantity'),
        ),
    ]
