from django.contrib.auth.models import User
from django.contrib.auth.signals import user_logged_out
from django.db.models import signals
from django.dispatch import receiver

from tastypie.models import create_api_key


@receiver(user_logged_out)
def sig_user_logged_out(sender, user, request, **kwargs):
    print user.api_key
    api_key_obj = user.api_key
    api_key_obj.delete()
    user.save()



