NAME_KEY = 'name'
PASSWORD_KEY = 'password'
EMAIL_KEY = 'email'
USERNAME_KEY = 'username'
API_ERROR_CODE = 'errorCode'
API_ERROR_MESSAGE = 'errorMessage'
DOES_NOT_EXIST = 'DoesNotExist'


#Files

REMOVED_SPACE_DRUG_NAME_AND_ORIGINAL_DRUG_NAME_MAPPING_FILE = 'space_removed_medicine_name_with_original_name'
BUILT_DRUG_TRIE_FILE = 'my_trie'