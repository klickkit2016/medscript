import constants
import json
import marisa_trie
import os

from django.contrib.auth import login
from django.contrib.auth.models import User

from tastypie.models import ApiKey

from medscript.models import Drug


def decode_json(str_data):
    try:
        return json.loads(str_data)
    except (ValueError, TypeError):
        raise JsonDecodeError()


def is_user_authentic(request):
    api_token = request.META.get('HTTP_AUTHORIZATION', None)
    if api_token is None:
        return (False, None)
    key_obj = ApiKey.objects.get(key=api_token)
    user = key_obj.user
    if user is not None:
        login(request, user)
        return (True, request.user)
    else:
        return (False, None)


def get_error_response(errorCode, errorMessage):
    return {
        constants.API_ERROR_CODE: errorCode,
        constants.API_ERROR_MESSAGE: errorMessage
    }


def get_json_from_file(file_name):
    with open(file_name) as json_data:
        return json.load(json_data)


def get_file_path(file_name):
    module_dir = os.path.dirname(__file__) # get current directory
    return os.path.join(module_dir, file_name)


def generate_token_for_user(user):
    key_obj = ApiKey(user=user)
    key_obj.save()
    return key_obj.key
