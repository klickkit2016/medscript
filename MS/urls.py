from django.conf.urls import url
from django.conf.urls import include
from django.contrib import admin

from tastypie.api import Api

from medscript.account_api import AccountResource
from medscript.api import DrugResource


v1_api = Api(api_name='v1')
v1_api.register(AccountResource())
v1_api.register(DrugResource())

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(v1_api.urls)),
]
